function findUsersInGermany(users)
{
   let usersFromGermany=[];

   for(const [key,value] of Object.entries(users))
   {

      if(value["nationality"].toLowerCase()==="germany")
         usersFromGermany.push(key);
   }

   return usersFromGermany;
}

module.exports=findUsersInGermany;