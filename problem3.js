function findUsersHaveMasters(users)
{
    let usersHavingMasters=[];

    for(const [key,value] of Object.entries(users))
    {
        if(value["qualification"].toLowerCase()==="masters")
            usersHavingMasters.push(key);
    }

    return usersHavingMasters;
}

module.exports=findUsersHaveMasters;