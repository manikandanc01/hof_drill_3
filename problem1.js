function findUserInterestedInVideoGames(users)
{    
    
    let usersInterestedInVideoGames=[];

    for(const [key,value] of Object.entries(users))
    { 
      if(value["interests"][0].includes("Video Games"))
       {
          usersInterestedInVideoGames.push(key);
       }    
    }

    return usersInterestedInVideoGames;
}

module.exports=findUserInterestedInVideoGames;
