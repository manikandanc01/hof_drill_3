function groupUsersByProgramming(users)
{
    
    let userGroups={
        'Python':[],
        'Javascript':[],
        'Golang':[]
    };

    for(const [key,value] of Object.entries(users))
    {

        for(const programName of Object.keys(userGroups))
        {
           
            if(value["desgination"].includes(programName))
            {
                userGroups[programName].push(key);
            }
        }
    }

    return userGroups;
}

module.exports=groupUsersByProgramming;